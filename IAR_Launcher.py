#!/usr/bin/env python3

import config
from pynput import keyboard
from time import sleep
import sys,os
from selenium import webdriver
from splinter.browser import Browser
import pyautogui
import base64
from time import sleep


def on_activate():
    print('Bypass key detected. Exiting launcher.')
    os.system("/usr/bin/startxfce4")
    return False
def for_canonical(f):
    return lambda k: f(l.canonical(k))

if not config.SCREEN['FORCEBYPASS'] :
    # !!!!  HERE BE DRAGONS. FOLLOW NOT AND RETURN TO WHENCE YOU CAME ELSE CHANCE LOSS OF LIMB OR LIFE!!! #
    os.environ["PATH"] += os.pathsep + "/usr/lib/chromium-browser/"  # Help find the chrome driver
    resolution = pyautogui.size()

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation']);
    chrome_options.add_argument("--disable-dev-shm-using")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--kiosk')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-save-password-bubble')
    chrome_options.add_argument('--window-size={},{}'.format(resolution.width, resolution.height))

    browser = Browser('chrome', headless=False, fullscreen=True, incognito=False, options=chrome_options)
    browser.visit("https://www.iamresponding.com/v3/Pages/memberlogin.aspx")
    #browser.find_by_id('subscriberLogin').click()
    try:
        browser.find_by_id('rcc-confirm-button').click()
        browser.visit("https://www.iamresponding.com/v3/Pages/memberlogin.aspx")
    except:
        pass


    browser.find_by_id('ddlsubsciribers').fill(config.IAR['AGENCY'])
    browser.find_by_id('memberfname').fill(config.IAR['USERNAME'])
    browser.find_by_id('memberpwd').fill(base64.b64decode(config.IAR['PASSWORD']).decode('utf-8'))
    browser.find_by_id('login').click()
else:
    os.system("/usr/bin/startxfce4")



# Watch for hotkey to start XFCE4 proper and a terminal. FOR DEBUGING USE ONLY. NOTHING NEFARIOUS PLEASE!! THANKS!
hotkey = keyboard.HotKey(
    keyboard.HotKey.parse('<ctrl>+<alt>+<esc>'), on_activate)
with keyboard.Listener(on_press=for_canonical(hotkey.press)) as l:
    l.join()
