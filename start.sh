#!/bin/bash

# Run this script in display 0 - the monitor
export DISPLAY=:0

# Prevents screen blanking
xset s off
xset s noblank
xset -dpms



gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.session idle-delay 0

python3 ./IAR_Launcher.py
