# RPIARScreen

An Ansible playbook and associated bits to take a Ubuntu 20.04 (maybe other versions..not tested) install (tested on an RPi 3b) and tweak some stuff to turn it into an [IAMRESPONDING](http://iamresponding.com) display.


## Setup and Installation

1. Have something with a fresh install of Ubuntu (preferably 20.04) on it.
2. Have a usernamed `ubuntu`. Either the default user in some cases or just another user
3. Have Ansible installed on another machine
4. `git clone` this repo and change directories into it
5. copy config.py.dist to config.py and edit it to your needs
6. Execute `ansible-playbook -i <Ubuntu machine>, setup_iar.yaml  -u <user> -k` where \<Ubuntu Machine\> is the hostname OR IP of the fresh box and \<user\> is some user that can sudo. The default RPi 20.04 image uses 'ubuntu'
7. You'll be prompted for a password for the user provided above, enter it
8. Sit back and wait. The box should reboot. If there are no errors continue on. If there are errors, submit an issue with as much info on it as you can
9. ssh into box with above user and execute `sudo x11vnc -storepasswd <Somepassword> /etc/x11vnc.pwd` where \<Somepassword\> is a good enough VNC password
10. `sudo systemctl restart lightdm`
11. You now have a working I Am Respinding kiosk. Enjoy.


## Contributing
Pull requests are welcome. Submitting an issue first with a description of what you are changing is preferred but not required

## License
[MIT](https://choosealicense.com/licenses/mit/)
